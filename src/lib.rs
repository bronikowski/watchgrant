#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {}
}

pub mod vagrant {

    //! A library dealing with Vagrant's configuration

    use std::fs::File;
    use std::io::prelude::*;
    use std::path::Path;
    extern crate json;

    /// Consumes a &str and emits a json::JsonValue to unwrap
    pub fn parse_json_string(json_content: &str) -> json::JsonValue {
        //println!("Parsing {}", json_content);
        let parsed = json::parse(json_content).unwrap();
        parsed
    }

    /// Loads a JSON file, emits a root object, if create is set to true, an empty file will be
    /// created
    pub fn load_configuration_content(file_name: &str, create: bool) -> Result<json::JsonValue, json::JsonValue> {

        if create {
            // ensure that file exists
            if !Path::new(file_name).exists() {
                return Ok(json::JsonValue::new_object());
            }
        }
        let mut json_content = String::new();
        let mut file = match File::open(file_name) {
            Err(e) => panic!(e),
            Ok(file) => file,
        };

        file.read_to_string(&mut json_content).unwrap();
        Ok(parse_json_string(json_content.as_str()))
    }

    /// FIXME: very suboptimal
    /// Stores a current state per machine
    pub fn set_current_state(file_name: &str, machine_id: &str, state: &str) {
        //println!("{}, {} → {}", file_name, machine_id, state);

        let current_state = load_configuration_content(file_name, true).unwrap();
        let mut object = json::object::Object::new();

        for e in current_state.entries() {
            let data = e.1.as_str().unwrap();
            object.insert(e.0, data.into());
        }
 
        object.insert(machine_id, state.into());
       let res = json::JsonValue::Object(object);

        let mut file = match File::create(file_name) {
            Ok(file) => file,
            Err(e) => panic!(e),
        };

        file.write(res.pretty(2).as_bytes()).expect("Can not save results");

    }
}
