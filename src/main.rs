
extern crate watchgrant;
extern crate json;
use watchgrant::vagrant::*;
fn main() {
    let results: json::JsonValue = load_configuration_content(&String::from("/home/emil/.vagrant.d/data/machine-index/index"),
                                                              false).unwrap();
    //println!("Debug: {}", results.pretty(2));
    for i in results.entries() {
        if i.0 != "machines" {
            continue;
        }
        let machines: &json::JsonValue = i.1;
        for m in machines.entries() {
            let machine_id = m.0;
            let data: &json::JsonValue = m.1;
            println!("Machine:     {}", machine_id);
            println!(" State:      {}", data["state"]);
            println!(" Local path: {}", data["local_data_path"]);
            set_current_state(&String::from("/home/emil/.vagrant.d/data/machine-index/watchgrant"),
                              machine_id,
                              data["state"].as_str().unwrap());
        }
    }
}
